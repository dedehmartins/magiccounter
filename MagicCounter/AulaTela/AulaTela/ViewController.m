//
//  ViewController.m
//  AulaTela
//
//  Created by BiraHH on 16/12/13.
//  Copyright (c) 2013 BiraHH. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
//  P1 things    -
@property (weak, nonatomic) IBOutlet UILabel *textLifeP1;
@property (weak, nonatomic) IBOutlet UILabel *textPoisonP1;

@property (weak, nonatomic) NSNumber *lifeP1;
@property (weak, nonatomic) NSNumber *poisonP1;
@property (weak, nonatomic) NSNumber *valueP1;
@property (weak, nonatomic) IBOutlet UIButton *attackP1;
@property (weak, nonatomic) IBOutlet UIButton *healP1;
@property (weak, nonatomic) IBOutlet UISwitch *poisonSwitchP1;
@property (weak, nonatomic) IBOutlet UIStepper *stepperP1;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerP1;


//  P2 things
@property (weak, nonatomic) IBOutlet UILabel *textLifeP2;
@property (weak, nonatomic) IBOutlet UILabel *textPoisonP2;
@property (weak, nonatomic) IBOutlet UIStepper *stepperP2;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerP2;

@property (weak, nonatomic) NSNumber *lifeP2;
@property (weak, nonatomic) NSNumber *poisonP2;
@property (weak, nonatomic) NSNumber *valueP2;
@property (weak, nonatomic) IBOutlet UISwitch *poisonSwitchP2;
@property (nonatomic) IBOutlet int P1;
@property (nonatomic) IBOutlet int P2;
@property (weak, nonatomic) IBOutlet UIButton *attackP2;
@property (weak, nonatomic) IBOutlet UIButton *healP2;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

@end

@implementation ViewController

-(void)viewDidLoad{
    [_attackP1 setBackgroundImage:[UIImage imageNamed:@"sword.png"] forState:UIControlStateNormal];
    [_attackP2 setBackgroundImage:[UIImage imageNamed:@"sword.png"] forState:UIControlStateNormal];
    [_healP2 setBackgroundImage:[UIImage imageNamed:@"heal.png"] forState:UIControlStateNormal];
    [_healP1 setBackgroundImage:[UIImage imageNamed:@"heal.png"] forState:UIControlStateNormal];
    _textLifeP1.layer.zPosition = 10000;
    _textLifeP2.layer.zPosition = 10000;
    [_backgroundImage sendSubviewToBack:_backgroundImage];
    _attackP1.layer.zPosition = 1000;
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    self.P1 = 20;
    self.P2 = 20;
    
    //  Rotaciona objetos na tela
    self.pickerP1.transform = CGAffineTransformMakeRotation(M_PI);
    self.stepperP1.transform = CGAffineTransformMakeRotation(M_PI);
    self.textLifeP1.transform = CGAffineTransformMakeRotation(M_PI);
    self.attackP1.transform = CGAffineTransformMakeRotation(M_PI);
    self.healP1.transform = CGAffineTransformMakeRotation(M_PI);
    self.textPoisonP1.transform = CGAffineTransformMakeRotation(M_PI);
    
}

- (IBAction)changePoison:(UISwitch *)sender {
    if (sender.isOn){
        if ([sender isEqual:_poisonSwitchP2])    [_attackP2 setBackgroundImage:[UIImage imageNamed:@"swordPoison.png"] forState:UIControlStateNormal];
        if ([sender isEqual:_poisonSwitchP1])    [_attackP1 setBackgroundImage:[UIImage imageNamed:@"swordPoison.png"] forState:UIControlStateNormal];
    }else{
                if ([sender isEqual:_poisonSwitchP2])    [_attackP2 setBackgroundImage:[UIImage imageNamed:@"sword.png"] forState:UIControlStateNormal];
                if ([sender isEqual:_poisonSwitchP1])    [_attackP1 setBackgroundImage:[UIImage imageNamed:@"sword.png"] forState:UIControlStateNormal];
    }
}
- (IBAction)valueChanged:(UIStepper *)sender {
    _P1 = [NSNumber numberWithDouble:(_stepperP1.value)].intValue;
    _P2 = [NSNumber numberWithDouble:(_stepperP2.value)].intValue;
    self.textLifeP2.text = [NSString stringWithFormat:@"%i",_P2];
    self.textLifeP1.text = [NSString stringWithFormat:@"%i",_P1];
}

- (IBAction)attackP2:(UIButton *)sender {
        // Fade out the view right away
        [UIView animateWithDuration:0.2
                              delay: 0.0
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             _attackP2.frame = CGRectMake(_attackP2.frame.origin.x-25, _attackP2.frame.origin.y-235,_attackP2.frame.size.width,_attackP2.frame.size.height);
                             CABasicAnimation *spin = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
                             spin.fromValue = [NSNumber numberWithFloat:M_PI+M_PI];
                             spin.toValue = [NSNumber numberWithFloat:M_PI/2+0.5+M_PI];
                             spin.duration = 0.21f;
                             spin.repeatCount = 1;
                             [_attackP2.layer addAnimation:spin forKey:@"mspin"];
                         }
                         completion:^(BOOL finished){
                             // Wait one second and then fade in the view
                             [self P2DealDamageToP1];
                             [self animDam1];
                             [UIView animateWithDuration:0.5
                                                   delay: 0.0
                                                 options:UIViewAnimationOptionCurveEaseOut
                                              animations:^{
                                                  _attackP2.frame = CGRectMake(_attackP2.frame.origin.x+25, _attackP2.frame.origin.y+235,_attackP2.frame.size.width,_attackP2.frame.size.height);
                                                  CABasicAnimation *spinBack = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
                                                  spinBack.fromValue = [NSNumber numberWithFloat:M_PI/2+0.5+M_PI];
                                                  spinBack.toValue = [NSNumber numberWithFloat:M_PI+M_PI];
                                                  spinBack.duration = 0.51f;
                                                  spinBack.repeatCount = 1;
                                                  [_attackP2.layer addAnimation:spinBack forKey:@"mspin2"];
                                              }
                                              completion:nil];
                         }];
    
    
    
    
    [UIView commitAnimations];

}

-(void)P2DealDamageToP1{
    if (self.poisonSwitchP2.isOn) {
        self.poisonP1 = @(self.poisonP1.intValue + [_pickerP2 selectedRowInComponent:0]+1);
        self.textPoisonP1.text = self.poisonP1.stringValue;
        _textPoisonP1.frame = CGRectMake(_textPoisonP1.frame.origin.x-5, _textPoisonP1.frame.origin.y,_textPoisonP1.frame.size.width,_textPoisonP1.frame.size.height);
        _textPoisonP1.frame = CGRectMake(_textPoisonP1.frame.origin.x+5, _textPoisonP1.frame.origin.y,_textPoisonP1.frame.size.width,_textPoisonP1.frame.size.height);
    }else{
        _P1 = (_P1 - ([_pickerP2 selectedRowInComponent:0]+1));
        self.textLifeP1.text = [NSString stringWithFormat:@"%i",_P1];
        _stepperP1.value=_P1;
        _textLifeP1.frame = CGRectMake(_textLifeP1.frame.origin.x+5, _textLifeP1.frame.origin.y,_textLifeP1.frame.size.width,_textLifeP1.frame.size.height);
        _textLifeP1.frame = CGRectMake(_textLifeP1.frame.origin.x-5, _textLifeP1.frame.origin.y,_textLifeP1.frame.size.width,_textLifeP1.frame.size.height);
        
    }
}
-(void)P1DealDamageToP2{
    if (self.poisonSwitchP1.isOn) {
        self.poisonP2 = @(self.poisonP2.intValue + [_pickerP1 selectedRowInComponent:0]+1);
        self.textPoisonP2.text = self.poisonP2.stringValue;
        _textPoisonP2.frame = CGRectMake(_textPoisonP2.frame.origin.x-5, _textPoisonP2.frame.origin.y,_textPoisonP2.frame.size.width,_textPoisonP2.frame.size.height);
        _textPoisonP2.frame = CGRectMake(_textPoisonP2.frame.origin.x+5, _textPoisonP2.frame.origin.y,_textPoisonP2.frame.size.width,_textPoisonP2.frame.size.height);
    }else{
        _P2 = (_P2 - ([_pickerP1 selectedRowInComponent:0]+1));
        self.textLifeP2.text = [NSString stringWithFormat:@"%i",_P2];
        _stepperP2.value=_P2;
        _textLifeP2.frame = CGRectMake(_textLifeP2.frame.origin.x+5, _textLifeP2.frame.origin.y,_textLifeP2.frame.size.width,_textLifeP2.frame.size.height);
        _textLifeP2.frame = CGRectMake(_textLifeP2.frame.origin.x-5, _textLifeP2.frame.origin.y,_textLifeP2.frame.size.width,_textLifeP2.frame.size.height);
        if([_pickerP1 selectedRowInComponent:0]+1>6){
            _pickerP2.frame = CGRectMake(_pickerP2.frame.origin.x+15, _pickerP2.frame.origin.y,_pickerP2.frame.size.width,_pickerP2.frame.size.height);
            _pickerP2.frame = CGRectMake(_pickerP2.frame.origin.x-15, _pickerP2.frame.origin.y,_pickerP2.frame.size.width,_pickerP2.frame.size.height);
            _stepperP2.frame = CGRectMake(_stepperP2.frame.origin.x+15, _stepperP2.frame.origin.y,_stepperP2.frame.size.width,_stepperP2.frame.size.height);
            _stepperP2.frame = CGRectMake(_stepperP2.frame.origin.x-15, _stepperP2.frame.origin.y,_stepperP2.frame.size.width,_stepperP2.frame.size.height);
        }
    }
}

- (IBAction)attackP1:(UIButton *)sender {
    
    
    [UIView animateWithDuration:0.2
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _attackP1.frame = CGRectMake(_attackP1.frame.origin.x+25, _attackP1.frame.origin.y+235,_attackP1.frame.size.width,_attackP1.frame.size.height);
                         CABasicAnimation *spin = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
                         spin.fromValue = [NSNumber numberWithFloat:M_PI];
                         spin.toValue = [NSNumber numberWithFloat:M_PI/2+0.5];
                         spin.duration = 0.21f;
                         spin.repeatCount = 1;
                         [_attackP1.layer addAnimation:spin forKey:@"mspin"];
                     }
                     completion:^(BOOL finished){
                         // Wait one second and then fade in the view
                         [self animDam2];
                         [self P1DealDamageToP2];
                         [UIView animateWithDuration:0.5
                                               delay: 0.0
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              _attackP1.frame = CGRectMake(_attackP1.frame.origin.x-25, _attackP1.frame.origin.y-235,_attackP1.frame.size.width,_attackP1.frame.size.height);
                                              CABasicAnimation *spinBack = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
                                              spinBack.fromValue = [NSNumber numberWithFloat:M_PI/2+0.5];
                                              spinBack.toValue = [NSNumber numberWithFloat:M_PI];
                                              spinBack.duration = 0.51f;
                                              spinBack.repeatCount = 1;
                                              [_attackP1.layer addAnimation:spinBack forKey:@"mspin2"];
                                          }
                                          completion:nil];
                     }];
}
- (IBAction)healP1:(UIButton *)sender {
    self.animHeal1;
    _P1 = (_P1 + ([_pickerP1 selectedRowInComponent:0]+1));
    self.textLifeP1.text = [NSString stringWithFormat:@"%i",_P1];
    _stepperP1.value = (double)_P1;
}

- (IBAction)healP2:(UIButton *)sender {
    self.animHeal2;
    _P2 = (_P2 + ([_pickerP2 selectedRowInComponent:0]+1));
    self.textLifeP2.text = [NSString stringWithFormat:@"%i",_P2];
    _stepperP2.value = (double)_P2;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 50;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [NSString stringWithFormat:@"%i",row+1];
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}


-(void)animDam1 {
    
    //  The fun begin
	CAEmitterLayer *emitterLayer = [CAEmitterLayer layer]; // Cria camada emissora de particulas
    
    //  Camada emissora
    emitterLayer.emitterPosition = CGPointMake(245, 130); // Posiciona na tela
    emitterLayer.emitterZPosition = -100; // Posicao no eixo Z
    emitterLayer.emitterSize = CGSizeMake(10, 10); // Tamanho da camada emissora
    emitterLayer.emitterShape = kCAEmitterLayerCircle;
    
    //  Particula emitida
    CAEmitterCell *emitterCell = [CAEmitterCell emitterCell]; // Cria particula
    emitterCell.scale = 1.0; // Tamanho
    emitterCell.scaleRange = 0.5; // Range de variaçao de tamanho
    emitterCell.scaleSpeed = 0.8;
    //emitterCell.spin = 12.2;
    //emitterCell.spinRange = 1.2;
    //emitterCell.emissionLongitude = 40;
    //emitterCell.emissionLatitude = 50;
    emitterCell.emissionRange = (CGFloat)M_PI; // Angulo de emissao
    emitterCell.lifetime = 0.15;
    emitterCell.lifetimeRange = 0.2;
    emitterCell.birthRate = 190;
    emitterCell.velocity = 8;
    emitterCell.velocityRange = 2;
    emitterCell.yAcceleration = 0;
    emitterCell.color = [[UIColor colorWithRed:1 green:0.2 blue:0.2 alpha:0.8]
                         CGColor];
    
    emitterCell.contents = (id)[[UIImage imageNamed:@"im4.png"] CGImage];
    
    emitterCell.timeOffset = 0.3;
    emitterLayer.emitterCells = [NSArray arrayWithObject:emitterCell];
    [self.view.layer addSublayer:emitterLayer];
    
}

-(void)animHeal1 {
    
    //  The fun begin
	CAEmitterLayer *emitterLayer = [CAEmitterLayer layer]; // Cria camada emissora de particulas
    
    //  Camada emissora
    emitterLayer.emitterPosition = CGPointMake(245, 130); // Posiciona na tela
    emitterLayer.emitterZPosition = -100; // Posicao no eixo Z
    emitterLayer.emitterSize = CGSizeMake(100, 100); // Tamanho da camada emissora
    emitterLayer.emitterShape = kCAEmitterLayerCircle;
    
    //  Particula emitida
    CAEmitterCell *emitterCell = [CAEmitterCell emitterCell]; // Cria particula
    emitterCell.scale = 0.05; // Tamanho
    emitterCell.scaleRange = 0.5; // Range de variaçao de tamanho
    emitterCell.scaleSpeed = 0.8;
    //emitterCell.spin = 12.2;
    //emitterCell.spinRange = 1.2;
    //emitterCell.emissionLongitude = 40;
    //emitterCell.emissionLatitude = 50;
    emitterCell.emissionRange = (CGFloat)M_PI; // Angulo de emissao
    emitterCell.lifetime = 0.35;
    emitterCell.lifetimeRange = 0.2;
    emitterCell.birthRate = 350;
    emitterCell.velocity = 8;
    emitterCell.velocityRange = 2;
    emitterCell.yAcceleration = 0;
    emitterCell.color = [[UIColor colorWithRed:0 green:0 blue:1 alpha:0.07]
                         CGColor];
    
    emitterCell.contents = (id)[[UIImage imageNamed:@"im6.png"] CGImage];
    
    emitterCell.timeOffset = 0.3;
    emitterLayer.emitterCells = [NSArray arrayWithObject:emitterCell];
    [self.view.layer addSublayer:emitterLayer];
    
}

-(void)animDam2 {
    
    //  The fun begin
	CAEmitterLayer *emitterLayer = [CAEmitterLayer layer]; // Cria camada emissora de particulas
    
    //  Camada emissora
    emitterLayer.emitterPosition = CGPointMake(65, 450); // Posiciona na tela
    emitterLayer.emitterZPosition = -100; // Posicao no eixo Z
    emitterLayer.emitterSize = CGSizeMake(10, 10); // Tamanho da camada emissora
    emitterLayer.emitterShape = kCAEmitterLayerCircle;
    
    //  Particula emitida
    CAEmitterCell *emitterCell = [CAEmitterCell emitterCell]; // Cria particula
    emitterCell.scale = 1.0; // Tamanho
    emitterCell.scaleRange = 0.5; // Range de variaçao de tamanho
    emitterCell.scaleSpeed = 0.8;
    //emitterCell.spin = 12.2;
    //emitterCell.spinRange = 1.2;
    //emitterCell.emissionLongitude = 40;
    //emitterCell.emissionLatitude = 50;
    emitterCell.emissionRange = (CGFloat)M_PI; // Angulo de emissao
    emitterCell.lifetime = 0.15;
    emitterCell.lifetimeRange = 0.2;
    emitterCell.birthRate = 190;
    emitterCell.velocity = 8;
    emitterCell.velocityRange = 2;
    emitterCell.yAcceleration = 0;
    emitterCell.color = [[UIColor colorWithRed:1 green:0.2 blue:0.2 alpha:0.8]
                         CGColor];
    
    emitterCell.contents = (id)[[UIImage imageNamed:@"im4.png"] CGImage];
    
    emitterCell.timeOffset = 0.3;
    emitterLayer.emitterCells = [NSArray arrayWithObject:emitterCell];
    [self.view.layer addSublayer:emitterLayer];
    
}


-(void)animHeal2 {
    
    //  The fun begin
	CAEmitterLayer *emitterLayer = [CAEmitterLayer layer]; // Cria camada emissora de particulas
    
    //  Camada emissora
    emitterLayer.emitterPosition = CGPointMake(65, 450); // Posiciona na tela
    emitterLayer.emitterZPosition = -100; // Posicao no eixo Z
    emitterLayer.emitterSize = CGSizeMake(100, 100); // Tamanho da camada emissora
    emitterLayer.emitterShape = kCAEmitterLayerCircle;
    
    //  Particula emitida
    CAEmitterCell *emitterCell = [CAEmitterCell emitterCell]; // Cria particula
    emitterCell.scale = 0.05; // Tamanho
    emitterCell.scaleRange = 0.5; // Range de variaçao de tamanho
    emitterCell.scaleSpeed = 0.8;
    //emitterCell.spin = 12.2;
    //emitterCell.spinRange = 1.2;
    //emitterCell.emissionLongitude = 40;
    //emitterCell.emissionLatitude = 50;
    emitterCell.emissionRange = (CGFloat)M_PI; // Angulo de emissao
    emitterCell.lifetime = 0.35;
    emitterCell.lifetimeRange = 0.2;
    emitterCell.birthRate = 350;
    emitterCell.velocity = 8;
    emitterCell.velocityRange = 2;
    emitterCell.yAcceleration = 0;
    emitterCell.color = [[UIColor colorWithRed:0 green:0 blue:1 alpha:0.07]
                         CGColor];
    
    emitterCell.contents = (id)[[UIImage imageNamed:@"im6.png"] CGImage];
    
    emitterCell.timeOffset = 0.3;
    emitterLayer.emitterCells = [NSArray arrayWithObject:emitterCell];
    [self.view.layer addSublayer:emitterLayer];
    
}



@end
