//
//  AppDelegate.h
//  AulaTela
//
//  Created by BiraHH on 16/12/13.
//  Copyright (c) 2013 BiraHH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
