//
//  ATTAnimations.h
//  AulaTela
//
//  Created by BiraHH on 09/01/14.
//  Copyright (c) 2014 BiraHH. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ATTAnimations : NSObject

-(void)animHeal1;
-(void)animHeal2;
-(void)animDam1;
-(void)animDam2;

@end
